<span id="{$HolderID.ATT}" class="form-group checkbox {$ExtraClass.ATT} {$MessageClass}">
    <label for="{$ID}">
        {$Field} {$Title}
    </label>
    <% include FormFieldMessage %>
    <% include FormFieldDescriptionSmall %>
</span>
