# Changelog

## 0.2.0 (2017-03-02)

* CWPT-369 add extend btn (Sacha Judd)
* Removed link title attr on mainNav subnavigation (Matias)
* CWPT-95 add better naming for footer class (newleeland)
* CWPT-356: Improve accessibility in page breadcrumbs. Fix $BaseHref typo. (Robbie Averill)
* CWPT-347: Adjust userforms jQuery validator configuration for Bootstrap 3 validation states (Robbie Averill)
* CWPT-346 This card has a comment to explain the js of how the alternative text works. (Sacha Judd)
* CWPT-306: Adjust for user forms, add UDF templates and tweak SASS for fieldset group and legends in UDF (Robbie Averill)
* CWPT-156 move variable from typography to variables and rename help-block to meta-info (Sacha Judd)
* FIX Move filter context to an include template (Robbie Averill)
* CWPT-348 move search bar above navigation on mobile (Matias)
* Adding results string based on the outcome produced by the filters on both news and events pages (Paul Jayme)
* feature/CWPT-345_sitemap FIX (Sacha Judd)
* FIX Define entire copy of bootstrap variable sheet with our customised values, remove modularisation (Robbie Averill)
* FIX Remove !default from variables that are not defaults (Robbie Averill)
* feature/CWPT-321_base-variables base-theme (Sacha Judd)
* FIX Update variable precendence by explicitly separating variables and manifests (Robbie Averill)
* CWPT-97 update markup so that container and Quicklinks are separated (Paul Jayme)
* Changed caret to fontAwesome icon (Matias)
* CWPT-94: Add lang attribute to language selector, and native selected language name (Robbie Averill)
* CWPT-318: Move features/quicklinks to its own include outside of the main content area (Robbie Averill)
* CWPT-320: Remove Jumbotron template and implement the carousel with Jumbotron styles (Robbie Averill)
* CWPT-303: Tweak documentation, add some missing sections and fix some typos/grammar (Robbie Averill)
* Changed margin to padding on site-header (Matias)

## 0.1.0 (2017-02-16)

* FIX Use correct path for importing node_module SCSS dependencies (Robbie Averill)
* CWPT-315: Export jQuery to allow the advanced theme to import it (Robbie Averill)
* FIX Add npm package name and version (Robbie Averill)
* Develop, Bug Fix: MainNavigation Fix, removed console log (Matias)
* Develop, Bug Fix, MainNavigation (Matias)
* feature/CWPT-296_Variables added variables globally (Sacha Judd)
* CWPT-297 removed extra padding on list, moved aria-expanded attr to ul instead of div in children, added li margin to avoid overlap plus icon (Matias)
* CWPT-292 Main Navigation (Matias)
* CWPT-284: Use Bootstrap form validation states on FormFields and their holders (Robbie Averill)
* CWPT-290_style_cleanup readability (Sacha Judd)
* Fix pagination bug with large set of data (Paul Jayme)
* CWPT-280: MainNav bug fix (Matias)
* Applied BS3 markup to registry (Paul Jayme)
* CWPT-214: Load nested sitemap data with AJAX (Robbie Averill)
* feature/CWPT-278_Skiplinks added PageNav (Sacha Judd)
* feature/CWPT-70_remove_sharelinks deleted share js (Sacha Judd)
* Add initial sitemap templates and styles (Paul Clarke)
* feature/CWPT-269_IFrame FIX border none (Sacha Judd)
* CWPT-274: Add "lint" command for linting Javascript with eslint (Robbie Averill)
* CWPT-269_iframe added template (Sacha Judd)
* Feedback changes (Christopher Pitt)
* Added note about browser validation (Christopher Pitt)
* Added note about tree dropdown field (Christopher Pitt)
* Updated readme (Christopher Pitt)
* Added docs for changing menus (Christopher Pitt)
* Added docs for changing form fields (Christopher Pitt)
* Added docs about customising fe files (Christopher Pitt)
* Added docs for modifying template files (Christopher Pitt)
* Added code style and linting docs (Christopher Pitt)
* Added site creation docs (Christopher Pitt)
* [hotfix] remove getMessage variable as we don't want it there. (Simon Erkelens)
* Removed unused function from mix config (Christopher Pitt)
* Add the role to the element. (Simon Erkelens)
* develop: bug fix onClick menuNav (Matias)
* feature/CWPT-235_Optionset_radio_buttons (Sacha Judd)
* CWPT-267: Fix linting issues (Matias)
* CWPT-263_news_events_classes (Mikaela Young)
* CWPT-205: Add mainNav tab and mouse events (Matias)
* remove hreflang, set lang to en-nz for all language strings. (Simon Erkelens)
* Added aria-label tag to Archive Dates (Paul Jayme)
* Accessibility disable defaults and add role and aria options (Simon Erkelens)
* CWPT-258: change search alert classes (Mikaela Young)
* Fixed header structure for News/Events pages (Paul Jayme)
* "Fixed Page.ss aside column for pages with no children" (Paul Jayme)
* CWPT-244_hide_footer (Mikaela Young)
* CWPT-247_search_alert: add alert role (Mikaela Young)
* CWPT-243_search_page_layout: removing unnecessary classes (Mikaela Young)
* feature/CWPT-234_checkdrop_aria (Sacha Judd)
* Fixed external links css (Paul Jayme)
* CWPT-225_cms_image_alignment: add typography and alignment styles to cms (Mikaela Young)
* Add lang="mi" to maori (Simon Erkelens)
* flatten (Simon Erkelens)
* feature/CWPT-223_Footer_logo (Sacha Judd)
* Form message fixes. (Simon Erkelens)
* Fixed Alt Text on Header/Logo (Paul Jayme)
* Updated page wordings. (Simon Erkelens)
* feature/CWPT-224: Fix pagination on page results (Matias)
* CWPT-84 Search messages (Simon Erkelens)
* CWPT-127: left align last modified text on print (Mikaela Young)
* CWPT-202: Language toggle icon fix (Firefox) (Matias)
* Fixed Button Spacing on News/Events Pages (Paul Jayme)
* feature/CWPT-146_breadcrumb_contrast (Sacha Judd)
* CWPT-194_optionset_checkboxset_classes (Mikaela Young)
* feature/CWPT-203_navbar_fix (Sacha Judd)
* feature/CWPT-136_quicklinks_spacing (Sacha Judd)
* Don't add aria-describedby if there is nothing that's describing (Simon Erkelens)
* Fix sr-only (Simon Erkelens)
* feature/CWPT-140_export added true for Pdflink/css (Sacha Judd)
* CWPT-174_event_holder_and_item: print styles (Mikaela Young)
* CWPT-177: focus overrides for dropdown (Matias)
* feature/CWPT-140_Header_nav_issues (Sacha Judd)
* CWPT-144_skip_links: reposition and style skip links (Mikaela Young)
* Added BS3 Styling to EventsHolder (Paul Jayme)
* Searchresult highlighting. (Simon Erkelens)
* CWPT-30_print_style_2 add print style (Mikaela Young)
* CWPT-158 Laorge search field (Simon Erkelens)
* CWPT-163, add print on search results (Matias)
* CWPT-143 (Simon Erkelens)
* Result fixes. (Simon Erkelens)
* NPM Build (Simon Erkelens)
* Added BS3 classes for Typography (Paul Jayme)
* feature/CWPT-135_Homepage_minor_tweaks created generic page utilities for across all pages (Sacha Judd)
* Searchform styling (Simon Erkelens)
* CWPT-47-redo-forms (Christopher Pitt)
* CWPT-41_search_results_theme: Added Search result style (Matias)
* feature/Homepage_style_fixes (Sacha Judd)
* Added BS3 Markup + Refactored NewsHolder (Sacha Judd)
* CWPT-88-home-page-styles (Christopher Pitt)
* Added translations (Christopher Pitt)
* CWPT-47 badly-named-o-tron (Christopher Pitt)
* Feedback changes (Christopher Pitt)
* Re-shuffled markup to support a better, responsive design (Christopher Pitt)
* Clean up header markup and styles (Christopher Pitt)
* CWPT-50 News Item layout (Simon Erkelens)
* feature/CWPT-87_Footer: Add Footer new style (Matias)
* override bootstrap styles added design (Sacha Judd)
* feature/CWPT-51 added BS3 markup to Pagination (Sacha Judd)
* Bug Fix, Fixed SidebarNavChildren.ss internal looping (Matias)
* [bugfix] the homepage didn't use the full width (Simon Erkelens)
* Moved form to new location (Christopher Pitt)
* feature/CWPT-54_Sidebar, Add Sidebar (Matias)
* [bugfix] main.js required ;`s at the end of import statements. (Simon Erkelens)
* CWPT-45 form holder (Christopher Pitt)
* Added BS3 styling to $SearchForm + Imported BS3 JS for Collapse/Tab (Paul Jayme)
* Homepage template with Quicklinks and Featured content. (Simon Erkelens)
* feature/CWPT-63_Favicon Add favicon (Matias)
* CWPT-53 related pages (Christopher Pitt)
* Refactored and Added BS3 Styling to Header.ss (Paul Jayme)
* CWPT-62 google analytics (Christopher Pitt)
* CWPT-48 last edited (Christopher Pitt)
* CWPT-44-footer-2 added title and attr on links and traslatable texts (Matias)
* CWPT-44-footer-2 added footer and image dir with gov logo in it (Matias)
* adding breadcrumbs styling (Sacha Judd)
* Unchanged (slightly syntax improved) templates (Simon Erkelens)
* CWPT-55 skip links (Christopher Pitt)
* Adding translations for share links (Christopher Pitt)
* Fix ie 9 window.open bug (Christopher Pitt)
* Ignored path: dist/css/main.css (Matias)
* CWPT-52 share links + font awesome (Christopher Pitt)
* Pushing built assets thanks to no NPM on UAT (Christopher Pitt)
* CWPT-68 Document Node requirements (Simon Erkelens)
* CWPT-39 added page templates (Christopher Pitt)
* CWPT-56 document Mix (Christopher Pitt)
* Updated composer to work the correct way on requiring theme module. (Simon Erkelens)
* CWPT-57 set up JS lint (Christopher Pitt)
* composer init (Simon Erkelens)
* CWPT-33 set up mix + include Bootstrap (Christopher Pitt)
* Initial commit (Christopher Pitt)
